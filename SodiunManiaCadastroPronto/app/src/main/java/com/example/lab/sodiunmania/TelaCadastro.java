package com.example.lab.sodiunmania;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.support.v7.app.AlertDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class TelaCadastro extends Activity implements View.OnClickListener {


    private EditText Nome, Porcao, Mporcao;
    private Button btn;
    private String FILENAME = "teste.txt";
    private AlertDialog alerta;
    private int j = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_cadastro);

        Nome = (EditText) findViewById(R.id.Nome);
        Porcao = (EditText) findViewById(R.id.Porcao);
        Mporcao = (EditText) findViewById(R.id.Mporcao);

        btn = findViewById(R.id.btnCadastro);
        btn.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
        if (v == btn) {

            FileOutputStream out = null;
            try {
                out = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                out.write(Nome.getText().toString().getBytes());
                out.write("\n".getBytes());
                out.write(Porcao.getText().toString().getBytes());
                out.write("\n".getBytes());
                out.write(Mporcao.getText().toString().getBytes());
                out.close();
                Log.d("####", "-> Deu certo!");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }




                if(j==3){

                    Intent i = new Intent(this, TelaCadastro2.class);
                    alerta();
                    startActivity(i);
                }

                j+=1;


            }







        }



    private void alerta(){
        //Cria o gerador do AlertDialog
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        //define o titulo
        builder.setTitle("Status");
        //define a mensagem
        builder.setMessage("Cadastrado com Sucesso!");

        alerta = builder.create();
        alerta.show();
    }


}




