package com.example.lab.projetosodiumcomparar14_08;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ifsuldeminas.projetosodiumcomparar14_08.R;

public class tela_comparar_02 extends AppCompatActivity implements View.OnClickListener {

    private EditText nome02;
    private EditText massa02, porcao02;
    private Button botaocomparar;
    private SharedPreferences shared;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_comparar_02);

        nome02 = findViewById(R.id.edNome2);
        massa02 = findViewById(R.id.massa2);
        porcao02 = findViewById(R.id.porcao2);

        botaocomparar = findViewById(R.id.botaocomparar);
        botaocomparar.setOnClickListener(this);

        shared = getSharedPreferences("comparar2", 0);
    }


    @Override
    public void onClick(View view) {

        if (nome02.getText().toString().length() == 0) {
            nome02.setError("Digite o nome!");
        }
        if(view == botaocomparar){
            Intent i = new Intent(this,telaComparar.class);
            //Bundle c = new Bundle();

            String nome2,massa2,porcao2 = "";

            nome2 = nome02.getText().toString();
            massa2 = massa02.getText().toString();
            porcao2 = porcao02.getText().toString();

            SharedPreferences.Editor edit = shared.edit();
            edit.putString("nome-2", nome2);
            edit.putString("massa-2", massa2);
            edit.putString("porcao-2", porcao2);
            edit.commit();

            /*c.putString("nome1", nome2);
            c.putString("massa1", massa2);
            c.putString("porcao1", porcao2);
            i.putExtras(c);*/
            startActivity(i);


        }

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
