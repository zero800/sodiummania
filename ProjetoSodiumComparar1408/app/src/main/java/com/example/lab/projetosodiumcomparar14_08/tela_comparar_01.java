package com.example.lab.projetosodiumcomparar14_08;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ifsuldeminas.projetosodiumcomparar14_08.R;

public class tela_comparar_01 extends AppCompatActivity implements View.OnClickListener {

    private EditText massa, porcao;
    private EditText nome1;
    private Button botaoavancar;
    private SharedPreferences shared;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_comparar_01);

        nome1 = findViewById(R.id.edNome1);
        massa = findViewById(R.id.massa);
        porcao = findViewById(R.id.porcao);

        botaoavancar = findViewById(R.id.botaoavancar);
        botaoavancar.setOnClickListener(this);

        shared = getSharedPreferences("comparar1", 0);

    }

    @Override
    public void onClick(View view) {

        if(view == botaoavancar){
            Intent i = new Intent(this,tela_comparar_02.class);
            Bundle b = new Bundle();
            String nome01 = "";
            String massa01 = "";
            String porcao01 = "";

            nome01 = nome1.getText().toString();
            massa01 = massa.getText().toString();
            porcao01 = porcao.getText().toString();

            /*b.putString("nome01", nome01);
            b.putString("massa01", massa01);
            b.putString("porcao01", porcao01);
            i.putExtras(b);*/

            SharedPreferences.Editor edit = shared.edit();
            edit.putString("nome-1", nome01);
            edit.putString("massa-1", massa01);
            edit.putString("porcao-1", porcao01);
            edit.commit();


            startActivity(i);

        }

    }

}
