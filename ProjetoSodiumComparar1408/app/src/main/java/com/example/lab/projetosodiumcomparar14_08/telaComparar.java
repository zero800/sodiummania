package com.example.lab.projetosodiumcomparar14_08;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import ifsuldeminas.projetosodiumcomparar14_08.R;

public class telaComparar extends AppCompatActivity implements View.OnClickListener {


    private TextView resultadotexto,nomeResultado,NomeMelhor,MassaMelhor, PorcaoMelhor, QPPMelhor, NomePior, MassaPior, PorcaoPior, QPPPior, textoprogresso,textoprogressoPior;
    private ProgressBar barradeprogresso, barradeprogressoPior;
    private SharedPreferences shared;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_comparar);

        shared = getSharedPreferences("comparar1", 0);

        String nome1 = shared.getString("nome-1", null);
        String massa = shared.getString("massa-1", null);
        String porcao = shared.getString("porcao-1", null);

        shared = getSharedPreferences("comparar2", 0);

        String nome2 = shared.getString("nome-2", null);
        String massa2 = shared.getString("massa-2", null);
        String porcao2 = shared.getString("porcao-2", null);

        textoprogresso = findViewById(R.id.textoprogresso);
        textoprogressoPior = findViewById(R.id.textoprogressoPior);
        nomeResultado = findViewById(R.id.NomeResultado);

        NomeMelhor = findViewById(R.id.NomeMelhor);
        MassaMelhor = findViewById(R.id.MassaMelhor);
        PorcaoMelhor = findViewById(R.id.PorcaoMelhor);
        QPPMelhor = findViewById(R.id.QPPMelhor);

        NomePior= findViewById(R.id.NomePior);
        MassaPior = findViewById(R.id.MassaPior);
        PorcaoPior = findViewById(R.id.PorcaoPior);
        QPPPior = findViewById(R.id.QPPPior);


        barradeprogresso = findViewById(R.id.barradeprogresso);
        barradeprogressoPior = findViewById(R.id.barradeprogressoPior);

        String resultado;
        int valorbarra;
        int valorbarra2;

        String nomeUm = nome1;
        String nomeDois = nome2;
        String m1 = massa;
        String p1 = porcao;

        Double massa01 = Double.valueOf(m1);
        Double porcao01 = Double.valueOf(p1);


        String m2 = massa2;
        String p2 = porcao2;

        Double massa02 = Double.valueOf(m2);
        Double porcao02 = Double.valueOf(p2);

        Double resultado01 = (massa01 / porcao01);
        Double resultado02 = (massa02 / porcao02);

        if (resultado01 > resultado02) {
            nomeResultado.setText("O melhor é: " + nomeDois);

            NomeMelhor.setText("Nome: " + nomeDois);
            MassaMelhor.setText("Massa: " + massa2);
            PorcaoMelhor.setText("Porção: " + porcao2);

            NomePior.setText("Nome: " + nomeUm);
            MassaPior.setText("Massa: " + massa);
            PorcaoPior.setText("Porção: " + porcao);

            String res = Double.toString(resultado01);
            String res2 = Double.toString(resultado02);

            QPPMelhor.setText("Quantidade de sódio por porção: " + res2);
            QPPPior.setText("Quantidade de sódio por porção: " + res);

        }


        if (resultado02 > resultado01) {

            NomeMelhor.setText("Nome: " +nomeUm);
            MassaMelhor.setText("Massa: " + massa);
            PorcaoMelhor.setText("Porcao: " + porcao);

            NomePior.setText("Nome: " + nomeDois);
            MassaPior.setText("Massa: " + massa2);
            PorcaoPior.setText("Porcao: " + porcao2);

            String res = Double.toString(resultado01);
            String res2 = Double.toString(resultado02);

            QPPMelhor.setText("Quantidade de sódio por porção: " + res);
            QPPPior.setText("Quantidade de sódio por porção: " + res2);


            nomeResultado.setText("O melhor é: " + nomeUm);
            resultado = "Quantidade de sódio por porção: " + resultado01;


            QPPMelhor.setText("Quantidade de sódio por porção: " + res);
        }

        if (resultado01 == resultado02) {
            NomeMelhor.setText("A concentração de sódio entre os dois alimentos é igual");
        }

        valorbarra = (int) Math.round((resultado01 * 100) / 2000);
        valorbarra2 = (int) Math.round((resultado02 * 100) / 2000);

        barradeprogresso.setProgress(valorbarra);
        barradeprogressoPior.setProgress(valorbarra2);

        textoprogresso.setText(String.valueOf(valorbarra) + "%");
        textoprogressoPior.setText(String.valueOf(valorbarra2) + "%");

    }


    @Override
    public void onClick(View view) {

                }
            }

